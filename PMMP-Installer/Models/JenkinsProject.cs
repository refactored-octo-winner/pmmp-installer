﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PMMP_Installer.Models
{

    public class JenkinsProject
    {
        public List<JenkinsBuild> builds { get; set; }
        public LastStableBuild lastStableBuild { get; set; }
    }

    public class LastStableBuild
    {
        public int number { get; set; }
    }

    public class JenkinsBuild
    {
        public List<JenkinsArtifact> artifacts { get; set; }
        public bool building { get; set; }
        public int number { get; set; }
        public string result { get; set; }
    }

    public class JenkinsArtifact
    {
        public string fileName { get; set; }
    }

}
