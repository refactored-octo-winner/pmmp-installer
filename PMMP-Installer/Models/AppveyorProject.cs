﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PMMP_Installer.Models
{


    public class AppveyorProject
    {
        public List<AppveyorBuild> builds { get; set; }
    }

    public class AppveyorBuild
    {
        public string version { get; set; }
        public string status { get; set; }
    }


}
