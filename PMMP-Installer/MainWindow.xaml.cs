﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO.Compression;
using PMMP_Installer.Models;
using RestSharp;
using System.Diagnostics;
using System.Net;
using System.IO;
using Ookii.Dialogs.Wpf;
using System.Reflection;

namespace PMMP_Installer
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private string baseUrl = "https://jenkins.pmmp.io";
        private string branch = "master";
        private string appveyorUser = "brandon15811";
        private string appveyorProjectName = "pmmp-installer";
        private int buildLimit = 20;
        private WebClient downloadClient;
        private IDictionary<string, Task<IRestResponse<JenkinsProject>>> jobData;
        private Assembly assembly;


        public MainWindow()
        {
            InitializeComponent();
            Main();
        }

        private async void Main()
        {
            disableControls();
            assembly = assembly = Assembly.GetExecutingAssembly();
            downloadClient = new WebClient();
            downloadClient.DownloadProgressChanged += downloadProgressChangedHandler;

            var apiClient = new RestClient(baseUrl);
            //var apiClient = new RestClient("http://requestb.in");

            var apiPath = "job/{job}/api/json?tree=builds[artifacts[fileName],number,building,result],lastStableBuild[number]";

            string[] jobList =
            {
                "PocketMine-MP",
                "PHP-Windows-x64",
                "PHP-Windows-x86",
                "PocketMine-MP Dev"
            };
            jobData = new Dictionary<string, Task<IRestResponse<JenkinsProject>>>();

            foreach (string job in jobList)
            {
                var jobRequest = new RestRequest(apiPath, Method.GET);
                jobRequest.AddUrlSegment("job", job);
                jobRequest.AddUrlSegment("buildLimit", buildLimit.ToString());
                jobData[job] = apiClient.ExecuteTaskAsync<JenkinsProject>(jobRequest);
            }

            Task<IRestResponse<JenkinsProject>[]> apiTasks = Task.WhenAll(jobData.Values.ToList());
            await apiTasks;

            if (jobData.Any(x => x.Value.Result.ResponseStatus != ResponseStatus.Completed))
            {
                ShowErrorDialog("Getting build list failed, please try again later");
                Application.Current.Shutdown();
                return;
            }

            Func<JenkinsBuild, bool> query = x => x.building == false && x.result == "SUCCESS";

            foreach (string job in jobList)
            {
                jobData[job].Result.Data.builds = jobData[job].Result.Data.builds.Where(query).Take(buildLimit).ToList();
            }

            pmmpVersionsList.ItemsSource = jobData["PocketMine-MP"].Result.Data.builds;
            pmmpVersionsList.SelectedItem = (pmmpVersionsList.ItemsSource as IEnumerable<JenkinsBuild>).First();
            stableRadioButton.IsChecked = true;

            if (Environment.Is64BitOperatingSystem)
            {
                phpVersionsList.ItemsSource = jobData["PHP-Windows-x64"].Result.Data.builds;
                x64RadioButton.IsChecked = true;
            }
            else
            {
                phpVersionsList.ItemsSource = jobData["PHP-Windows-x86"].Result.Data.builds;
                x86RadioButton.IsChecked = true;
            }
            phpVersionsList.SelectedItem = (phpVersionsList.ItemsSource as IEnumerable<JenkinsBuild>).First();

            var appveyorApiClient = new RestClient("http://ci.appveyor.com/api");

            var appveyorProjectRequest = new RestRequest("projects/{appveyorUser}/{appveyorProjectName}/history?recordsNumber={buildLimit}", Method.GET);
            appveyorProjectRequest.AddUrlSegment("appveyorUser", appveyorUser);
            appveyorProjectRequest.AddUrlSegment("appveyorProjectName", appveyorProjectName);
            appveyorProjectRequest.AddUrlSegment("buildLimit", buildLimit.ToString());

            var appVeyorProject = await appveyorApiClient.ExecuteTaskAsync<AppveyorProject>(appveyorProjectRequest);
            if (appVeyorProject.ResponseStatus == ResponseStatus.Completed)
            {
                var filteredAppveyor = appVeyorProject.Data.builds.Where(x => x.status == "success");
                Version currentVersion = assembly.GetName().Version;
                Version appveyorVersion = new Version(filteredAppveyor.First().version);
                if (currentVersion.CompareTo(appveyorVersion) < 0)
                {
                    if (ShowOKCancelDialog("New installer build is available. Click Ok to go to the download page.", "Update Available").ButtonType == ButtonType.Ok)
                    {
                        OpenUri(String.Format("https://ci.appveyor.com/project/{0}/{1}/build/artifacts", appveyorUser, appveyorProjectName));
                    }
                }
            } else
            {
                ShowWarningDialog("Checking for installer updates failed. Installer may be out of date.");
            }
            enableControls();
            downloadButton.IsEnabled = false;
        }

        #region Utils
        private string getArtifactUrl(string job, string buildNumber, string fileName)
        {
            return String.Format("{0}/job/{1}/{2}/artifact/{3}",
                         baseUrl, job, buildNumber, fileName);
        }

        private string getGithubUrl(string organization, string project, string branch, string path)
        {
            return String.Format("https://raw.githubusercontent.com/{0}/{1}/{2}/{3}",
                         organization, project, branch, path);
        }

        private Task downloadFile(string url, string folder, string fileName)
        {
            currentFileLabel.Content = fileName;
            return downloadClient.DownloadFileTaskAsync(url, folder + "\\" + fileName);
        }

        private bool IsDirectoryEmpty(string path)
        {
            return !Directory.EnumerateFileSystemEntries(path).Any();
        }

        private void ShowErrorDialog(string message, string extraMessage = null, string title = "Error")
        {
            var dialog = new TaskDialog();
            dialog.MainIcon = TaskDialogIcon.Error;
            dialog.WindowTitle = title;
            dialog.MainInstruction = message;
            if (extraMessage != null)
            {
                dialog.ExpandedInformation = extraMessage;
            }
            var okButton = new TaskDialogButton(ButtonType.Ok);
            dialog.Buttons.Add(okButton);
            dialog.ShowDialog();
        }

        private void ShowWarningDialog(string message, string extraMessage = null, string title = "Warning")
        {
            var dialog = new TaskDialog();
            dialog.MainIcon = TaskDialogIcon.Warning;
            dialog.WindowTitle = title;
            dialog.MainInstruction = message;
            if (extraMessage != null)
            {
                dialog.ExpandedInformation = extraMessage;
            }
            var okButton = new TaskDialogButton(ButtonType.Ok);
            dialog.Buttons.Add(okButton);
            dialog.ShowDialog();
        }

        private void ShowInfoDialog(string message, string extraMessage = null, string title = "Info")
        {
            var dialog = new TaskDialog();
            dialog.MainIcon = TaskDialogIcon.Information;
            dialog.WindowTitle = title;
            dialog.MainInstruction = message;
            if (extraMessage != null)
            {
                dialog.ExpandedInformation = extraMessage;
            }
            var okButton = new TaskDialogButton(ButtonType.Ok);
            dialog.Buttons.Add(okButton);
            dialog.ShowDialog();
        }

        private TaskDialogButton ShowOKCancelDialog(string message, string title, TaskDialogIcon icon = TaskDialogIcon.Warning)
        {
            var dialog = new TaskDialog();
            dialog.MainIcon = icon;
            dialog.WindowTitle = title;
            dialog.MainInstruction = message;
            var okButton = new TaskDialogButton(ButtonType.Ok);
            var cancelButton = new TaskDialogButton(ButtonType.Cancel);
            dialog.Buttons.Add(okButton);
            dialog.Buttons.Add(cancelButton);
            return dialog.ShowDialog();
        }

        private void disableControls()
        {
            downloadButton.IsEnabled = false;
            phpVersionsList.IsEnabled = false;
            pmmpVersionsList.IsEnabled = false;
            browseButton.IsEnabled = false;
            x64RadioButton.IsEnabled = false;
            x86RadioButton.IsEnabled = false;
            stableRadioButton.IsEnabled = false;
            devRadioButton.IsEnabled = false;
        }

        private void enableControls()
        {
            downloadButton.IsEnabled = true;
            phpVersionsList.IsEnabled = true;
            pmmpVersionsList.IsEnabled = true;
            browseButton.IsEnabled = true;
            x64RadioButton.IsEnabled = true;
            x86RadioButton.IsEnabled = true;
            stableRadioButton.IsEnabled = true;
            devRadioButton.IsEnabled = true;

            currentFileLabel.Content = "None";
            downloadProgressBar.Value = 0;
            percentDoneLabel.Content = 0;
        }

        //From http://stackoverflow.com/a/33573227
        public static bool IsValidUri(string uri)
        {
            if (!Uri.IsWellFormedUriString(uri, UriKind.Absolute))
                return false;
            Uri tmp;
            if (!Uri.TryCreate(uri, UriKind.Absolute, out tmp))
                return false;
            return tmp.Scheme == Uri.UriSchemeHttp || tmp.Scheme == Uri.UriSchemeHttps;
        }

        //From http://stackoverflow.com/a/33573227
        public static bool OpenUri(string uri)
        {
            if (!IsValidUri(uri))
                return false;
            Process.Start(uri);
            return true;
        }

        public static bool OpenFolder(string folder)
        {
            if (Directory.Exists(folder))
            {
                Process.Start("explorer.exe", folder);
                return true;
            }
            return false;
        }
        #endregion

        #region Events
        private void browseButton_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new VistaFolderBrowserDialog();
            if (dialog.ShowDialog(this).GetValueOrDefault())
            {
                folderPath.Content = dialog.SelectedPath;
                downloadButton.IsEnabled = true;
            }
        }

        private async void downloadButton_Click(object sender, RoutedEventArgs e)
        {
            disableControls();
            var pmmpBuildItem = ((JenkinsBuild)pmmpVersionsList.SelectedItem);
            var phpBuildItem = ((JenkinsBuild)phpVersionsList.SelectedItem);
            string selectedFolder = folderPath.Content.ToString();
            string phpProject = x64RadioButton.IsChecked == true ? "PHP-Windows-x64" : "PHP-Windows-x86";
            string pmmpProject = stableRadioButton.IsChecked == true ? "PocketMine-MP" : "PocketMine-MP Dev";

            var pmmpUrl = getArtifactUrl(pmmpProject, pmmpBuildItem.number.ToString(), pmmpBuildItem.artifacts[0].fileName);
            var phpUrl = getArtifactUrl(phpProject, phpBuildItem.number.ToString(), phpBuildItem.artifacts[0].fileName);

            if (!Directory.Exists(selectedFolder))
            {
                ShowErrorDialog("Selected folder \"" + selectedFolder + "\" does not exist");
                enableControls();
                return;
            }

            if (!IsDirectoryEmpty(selectedFolder))
            {
                string message = "Since \"" + selectedFolder + "\" is not an empty folder, some of the files in it may be deleted or overwritten. Continue?";
                if (ShowOKCancelDialog(message, "Continue?").ButtonType == ButtonType.Cancel)
                {
                    enableControls();
                    return;
                }
            }

            try
            {
                await downloadFile(pmmpUrl, selectedFolder, "PocketMine-MP.phar");
                await downloadFile(phpUrl, selectedFolder, phpBuildItem.artifacts[0].fileName);

                string[] githubFiles =
                {
                    "start.cmd",
                    "LICENSE",
                    "README.md",
                    "CONTRIBUTING.md"
                };
                foreach (string file in githubFiles)
                {
                    var githubUrl = getGithubUrl("pmmp", "PocketMine-MP", branch, file);
                    await downloadFile(githubUrl, selectedFolder, file);
                }
            }
            catch (WebException error)
            {
                ShowErrorDialog("Failed to download files", error.Message);
                enableControls();
                return;
            }

            try
            {
                string binFolder = selectedFolder + "\\bin";
                if (Directory.Exists(binFolder))
                {
                    Directory.Delete(binFolder, true);
                }
                ZipFile.ExtractToDirectory(selectedFolder + "\\" + phpBuildItem.artifacts[0].fileName, selectedFolder);
                File.Delete(selectedFolder + "\\" + phpBuildItem.artifacts[0].fileName);
            }
            catch (IOException error)
            {
                ShowErrorDialog("Failed to extract PHP: " + error.Message);
                enableControls();
                return;
            }

            enableControls();
            currentFileLabel.Content = "All done!";
            var openButtonMessage = "Install complete. Run start.cmd in " + selectedFolder + " to run the server. Click Ok to open the folder";
            ButtonType openButtonStatus = ShowOKCancelDialog(openButtonMessage, "Install Complete").ButtonType;
            if (openButtonStatus == ButtonType.Ok)
            {
                OpenFolder(selectedFolder);
            }
        }

        private void downloadProgressChangedHandler(object sender, DownloadProgressChangedEventArgs e)
        {
            downloadProgressBar.Value = e.ProgressPercentage;
            percentDoneLabel.Content = e.ProgressPercentage;
        }

        private void phpVersionsList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var cb = sender as ComboBox;
            var buildItem = ((JenkinsBuild)cb.SelectedItem);

            if (buildItem == null)
            {
                phpVersionFilename.Content = "No version selected";
                return;
            }

            phpVersionFilename.Content = buildItem.artifacts[0].fileName;
        }

        private void pmmpVersionsList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var cb = sender as ComboBox;
            var buildItem = ((JenkinsBuild)cb.SelectedItem);

            if (buildItem == null)
            {
                pmmpVersionFilename.Content = "No version selected";
                return;
            }

            pmmpVersionFilename.Content = buildItem.artifacts[0].fileName.ReplaceSecond("_", "_\n");
        }

        private void x86RadioButton_Checked(object sender, RoutedEventArgs e)
        {
            RadioButton btn = (RadioButton)sender;
            if (btn.IsChecked == true)
            {
                phpVersionsList.SelectedItem = null;
                phpVersionsList.ItemsSource = jobData["PHP-Windows-x86"].Result.Data.builds;
                phpVersionsList.SelectedItem = (phpVersionsList.ItemsSource as IEnumerable<JenkinsBuild>).First();
            }
        }

        private void x64RadioButton_Checked(object sender, RoutedEventArgs e)
        {
            RadioButton btn = (RadioButton)sender;
            if (btn.IsChecked == true)
            {
                phpVersionsList.SelectedItem = null;
                phpVersionsList.ItemsSource = jobData["PHP-Windows-x64"].Result.Data.builds;
                phpVersionsList.SelectedItem = (phpVersionsList.ItemsSource as IEnumerable<JenkinsBuild>).First();
            }
        }

        private void aboutMenuOption_Click(object sender, RoutedEventArgs e)
        {
            Window aboutWin = new AboutWindow();
            aboutWin.Owner = this;
            aboutWin.ShowInTaskbar = false;
            aboutWin.ShowDialog();
        }

        private void stableRadioButton_Checked(object sender, RoutedEventArgs e)
        {
            RadioButton btn = (RadioButton)sender;
            if (btn.IsChecked == true)
            {
                pmmpVersionsList.SelectedItem = null;
                pmmpVersionsList.ItemsSource = jobData["PocketMine-MP"].Result.Data.builds;
                pmmpVersionsList.SelectedItem = (pmmpVersionsList.ItemsSource as IEnumerable<JenkinsBuild>).First();
            }
        }

        private void devRadioButton_Checked(object sender, RoutedEventArgs e)
        {
            RadioButton btn = (RadioButton)sender;
            if (btn.IsChecked == true)
            {
                pmmpVersionsList.SelectedItem = null;
                pmmpVersionsList.ItemsSource = jobData["PocketMine-MP Dev"].Result.Data.builds;
                pmmpVersionsList.SelectedItem = (pmmpVersionsList.ItemsSource as IEnumerable<JenkinsBuild>).First();
            }
        }
    }

    public static class StringExtensionMethods
    {
        public static string ReplaceSecond(this string text, string search, string replace)
        {
            int pos = text.IndexOf(search, text.IndexOf(search) + 1);
            if (pos < 0)
            {
                return text;
            }
            return text.Substring(0, pos) + replace + text.Substring(pos + search.Length);
        }
    }
    #endregion
}
