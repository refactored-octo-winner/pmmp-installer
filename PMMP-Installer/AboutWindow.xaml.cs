﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PMMP_Installer
{
    /// <summary>
    /// Interaction logic for AboutWindow.xaml
    /// </summary>
    public partial class AboutWindow : Window
    {
        private Assembly assembly;
        private string assemblyPath;

        public AboutWindow()
        {
            InitializeComponent();
            assembly = Assembly.GetExecutingAssembly();
            versionLabel.Content = assembly.GetName().Version;
            assemblyPath = "PMMP_Installer.Licenses.";
            var filenames = assembly.GetManifestResourceNames()
                .Where(x => x.StartsWith(assemblyPath) && x.EndsWith(".txt"));
            licenseComboBox.ItemsSource = filenames.Select(x => x.Replace(assemblyPath, "").Replace(".txt", ""));
        }

        private string getResource(string fileName)
        {
            var text = new StreamReader(assembly.GetManifestResourceStream(assemblyPath + fileName));
            return text.ReadToEnd();
        }

        private void licenseComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var cb = sender as ComboBox;
            var fileName = ((string)cb.SelectedItem);
            licenseTextBox.Text = getResource(fileName + ".txt");
        }
    }
}
